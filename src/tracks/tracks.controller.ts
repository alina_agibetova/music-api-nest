import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Query,
} from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { Track, TrackDocument } from '../schemas/track.schema';
import { CreateTrackDto } from './create-track.dto';

@Controller('tracks')
export class TracksController {
  constructor(
    @InjectModel(Track.name) private trackModel: Model<TrackDocument>,
  ) {}

  @Get()
  getAll(@Query('album') album: string) {
    if (album) {
      return this.trackModel.find({ album });
    } else {
      return this.trackModel.find();
    }
  }

  @Post()
  async create(@Body() trackDto: CreateTrackDto) {
    const track = new this.trackModel({
      title: trackDto.title,
      album: trackDto.album,
      lengthTrack: trackDto.lengthTrack,
    });
    await track.save();
    return track;
  }

  @Get(':id')
  getOne(@Param('id') id: string) {
    return this.trackModel.findById(id);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.trackModel.deleteOne({ id });
  }
}
