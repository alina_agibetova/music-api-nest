export class CreateTrackDto {
  title: string;
  album: string;
  lengthTrack: string;
}
