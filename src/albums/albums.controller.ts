import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Query,
  UploadedFile,
  UseInterceptors,
} from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { Album, AlbumDocument } from '../schemas/album.schema';
import { CreateAlbumDto } from './create-album.dto';
import { FileInterceptor } from '@nestjs/platform-express';

@Controller('albums')
export class AlbumsController {
  constructor(
    @InjectModel(Album.name) private albumModel: Model<AlbumDocument>,
  ) {}
  @Get()
  getAll(@Query('artist') artist: string) {
    if (artist) {
      return this.albumModel.find({ artist });
    } else {
      return this.albumModel.find();
    }
  }
  @Get(':id')
  getOne(@Param('id') id: string) {
    return this.albumModel.findById(id);
  }
  @Post()
  @UseInterceptors(FileInterceptor('image', { dest: '.public/uploads/albums' }))
  async create(
    @UploadedFile() file: Express.Multer.File,
    @Body() albumDto: CreateAlbumDto,
  ) {
    const album = new this.albumModel({
      name: albumDto.name,
      artist: albumDto.artist,
      year: albumDto.year,
      image: file ? '/uploads/albums/' + file.filename : null,
    });
    await album.save();
    return album;
  }
  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.albumModel.deleteOne({ id });
  }
}
