export class CreateAlbumDto {
  name: string;
  artist: string;
  year: string;
}
