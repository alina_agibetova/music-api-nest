import mongoose, { Document } from 'mongoose';
import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';

export type TrackDocument = Track & Document;
@Schema()
export class Track {
  @Prop()
  title: string;
  @Prop({ ref: 'Album', required: true })
  album: mongoose.Schema.Types.ObjectId;
  @Prop()
  lengthTrack: string;
}

export const TrackSchema = SchemaFactory.createForClass(Track);
